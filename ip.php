<?
/*

Author: Karl Dyson <hackery@perlbitch.com>

Where I've borrowed ideas from googling I've included the blog URL etc

Partly inspired by wanting to learn more about making ajax calls, and write some javascript
and partly inspired by a mailing list thread on "can a webpage simultaneously display both
IPv4 & IPv6 for dual stack clients."

Comments, feedback welcome.

* Curl, WGet & libwww (LWP) get a plain text output with no HTML. Useful from the CLI.
* MSIE 6,7,8 & 9 get plain text output.
* Browsers that don't support javascript (links, lynx, etc) or have it disabled, get plain text
* Regular browsers that use ?plain text query string get plain text
* ?json query string gets the output in JSON format
* Regular browsers get IPv4 & IPv6 in one screen, GeoIP details for IPs including proxies etc
  and detection of whether a browser prefers A or AAAA given the choice.

Requires:

* The country code file as listed in the code; I basically grabbed the info kindly supplied
  in this SO answer http://stackoverflow.com/a/5396003 and chucked it in a file.
* memcached daemon, and php5 support (apt-get install memcached php5-memcached)

*/

// Parameters changeable by the user
// The general viewing of the site is expected to be to http://$siteHost/$sitePath
// The script will call out to $fourPrefix.$siteHost/$sitePath?json for the IPv4 and 
// similarly for the IPv6 check.
// It is required that host $fourPrefix.$siteHost ONLY has an A record in DNS
// It is required that host $sixPrefix.$siteHost ONLY has an AAAA record in DNS

$siteHost = 'c.je'; // The host part of the URL
$sitePath = 'ip'; // The path part of the URL
$fourPrefix = 4; // The host A record for the IPv4 URL (appended to siteHost
$sixPrefix = 6; // The host AAAA record for the IPv6 URL (appended to the siteHost
$rblZone = 'geo.rbl.junesta.net'; // RBL zone - shold return a TXT record with country code for IP. I'm using nerd.dk in rbldns.
$countryNamesFile = '/usr/local/etc/countrynames'; // location of the countrynames file

$siteURL = "http://".$siteHost.($sitePath ? "/$sitePath" : "");

// Lets start with memcache
$m = new Memcached();
$m->addServer('localhost', 11211);

// stdHeaders outputs, er, standard headers for all output types.
function stdHeaders() {
	global $siteHost;
	//set headers to NOT cache a page
	header("Access-Control-Allow-Origin: http://$siteHost");
	header("Cache-Control: no-cache, must-revalidate"); //HTTP 1.1
	header("Pragma: no-cache"); //HTTP 1.0
	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
	//or, if you DO want a file to cache, use:
	//header("Cache-Control: max-age=2592000"); //30days (60sec * 60min * 24hours * 30days)
}

// getCountry looks in memcache, and then in a file to get a country name from a country code.
function getCountry($cc) {
	global $m;
	if(!($c = $m->get($cc))) {
		//return;
		error_log("Didn't find cc $cc in Memcache");
		if($m->getResultCode() == Memcached::RES_NOTFOUND) {
			error_log("Memcache said NOTFOUND for $cc");
			// countrynames file is from http://stackoverflow.com/a/5396003
			// It's formated per line: Country Name;Code
			$countryNameHandle = fopen($countryNamesFile, "r");
			if($countryNameHandle) {
				while(($line = fgets($countryNameHandle)) !== false) {
					if(preg_match('/^(.*?);(\w+)$/', $line, $matches)) {
						$countryname = ucwords(strtolower($matches[1]));
						$countrycode = strtoupper($matches[2]);
						$countries[$countrycode] = $countryname;
						$m->set($countrycode, $countryname);
					}
				}
				fclose($countryNameHandle);
			}
		}
		else {
			error_log("Memcache said ".getResultCode()." for $cc");
		}
		return $countries[$cc];
	}
	else {
		return $c;
	}
}

// outputPlain outputs the data in plain text format
function outputPlain($data) {
	echo "Your IP is ".$data['REMOTE_ADDR']."\n";
	if(isset($data['REMOTE_ADDR_PTR'])) echo "DNS says that's ".$data['REMOTE_ADDR_PTR']."\n";
	if(isset($data['HTTP_VIA'])) {
		echo "You appear to be coming via a proxy: ".$data['HTTP_VIA']."\n";
		if(isset($data['HTTP_X_FORWARDED_FOR'])) {
			echo "It says you're ".$data['HTTP_X_FORWARDED_FOR']."\n";
		}
	}
	if(isset($data['COUNTRY_NAME'])) echo "You appear to be in ".$data['COUNTRY_NAME']." (".$data['COUNTRY'].")\n";
	if(isset($data['HTTP_USER_AGENT'])) echo "You're using ".$data['HTTP_USER_AGENT']."\n";
}

// OK, so start building the data used for plain text & json output...
$data = [];

// small helper function that adds a _SERVER entry to $data if suitable
function add($var) {
	global $data;
	if(isset($_SERVER[$var]) && !is_null($_SERVER[$var]))
		$data[$var] = $_SERVER[$var];
}

// add some standard headers
add('HTTP_USER_AGENT');
add('REMOTE_ADDR');
add('REMOTE_PORT');
add('HTTP_VIA');
add('HTTP_X_FORWARDED');

// lookup the ptr, and if there is one, add an entry for it to $data
$ptr = gethostbyaddr($_SERVER['REMOTE_ADDR']);
if($ptr !== $_SERVER['REMOTE_ADDR'])
	$data['REMOTE_ADDR_PTR'] = $ptr;

// If it's IPv4, set the version and lookup country code from our rbl
if(preg_match('/^\d+\.\d+\.\d+\.\d+$/', $_SERVER['REMOTE_ADDR'])) {
	$ipAddr = $_SERVER['REMOTE_ADDR'];
	global $m;
	if(!($countryCode = $m->get($ipAddr."_cc"))) {
		error_log("Didn't find cc for $ipAddr in Memcache");
		if($m->getResultCode() == Memcached::RES_NOTFOUND) {
			error_log("Memcache said NOTFOUND for cc for $ipAddr");

			$rip = join('.', array_reverse(explode('.', $_SERVER['REMOTE_ADDR'])));
			$country = dns_get_record("$rip.$rblZone", DNS_TXT);
			$countryCode = strtoupper(trim($country[0]['txt']));
			$m->set($ipAddr."_cc", $countryCode, 60*60*24*14);
		}
		else {
			error_log("Memcache said ".getResultCode()." for $cc");
			//error_log("Memcache unhappy for $cc");
		}
	}
	$countryName = getCountry($countryCode);
	$data['COUNTRY'] = $countryCode;
	$data['COUNTRY_NAME'] = $countryName;
	$data['IP_VERSION'] = 4;
}
// otherwise it's IPv6...
else {
	$data['IP_VERSION'] = 6;
}

// is it proxied? If so, add the original header, as well as an array of IPs
// also add a same-size array of PTRs (or NULL, for padding)
if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !is_null($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	$xffarr = explode(', ', $_SERVER['HTTP_X_FORWARDED_FOR']);
	foreach($xffarr as $f) {
		$xff[] = $f;
		$p = gethostbyaddr($f);
		$xff_ptr[] = $p === $f ? NULL : $p;
	}
	$data['HTTP_X_FORWARDED_FOR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
	$data['XFF'] = $xff;
	$data['XFF_PTR'] = $xff_ptr;
}

if(preg_match('/^([a-zA-Z0-9]+)\.i\.p\.c\.je$/', $_SERVER['HTTP_HOST'], $host_matches)) {
	$id = $host_matches[1];
	$source = $m->get("ipdns_$id");
	$data['IPDNS']['ID'] = $id;
	$data['IPDNS']['SOURCE'] = $source;
	$client = $m->get("ipdns_client_$id");
	$data['IPDNS']['CLIENT'] = $client;
}

// ACTUALLY START OUTPUTTING STUFF

// If the request is for JSON, output std headers and JSON
if(preg_match('/^json$/', $_SERVER['QUERY_STRING'])) {
	stdHeaders();
	header("Content-Type: application/json");
	echo json_encode($data);
	//echo json_encode($data, JSON_PRETTY_PRINT);
	exit;
}
// If client is shit, er, I mean, MSIE 6, 7, 8 or 9, then plain text output with sarcasm
if(preg_match('/MSIE [6789]/', $_SERVER['HTTP_USER_AGENT'])) {
	stdHeaders();
	header("Content-Type: text/plain");
	echo "Your browser is old and crusty, and should have been upgraded a *long* time ago.\n\n";
	outputPlain($data);
	exit;
}
// If client is curl, wget, libwww etc, or *asks* for plain, then plain text output.
// This is after the MSIE hack to ensure they get the sarcasm either way
if(preg_match('/^(curl|Wget|libwww)/', $_SERVER['HTTP_USER_AGENT']) || preg_match('/^plain$/', $_SERVER['QUERY_STRING'])) {
	stdHeaders();
	header("Content-Type: text/plain");
	outputPlain($data);
	exit;
}
// If we got this far, the client is gonna get the 'clever' site, so if the HTTP_HOST is not $siteHost
// then redirect to that, else the origin thing barfs.
if($_SERVER['HTTP_HOST'] !== $siteHost) header("Location: $siteURL\r\n\r\n");

// Here ends the PHP, let the HTML & JS begin...
?>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<title>Your IP Address</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<link rel="stylesheet" href="/css/ip.php.css" type="text/css" media="screen,projection" />
</head>
<body>
<h1>Your IP Address</h1>
<div id=content>
<noscript>
<pre>
Your browser doesn't appear to support Javascript.
<?
// Chuck the plain text version in the noscript section for browsers without JS, or with it disabled
outputPlain($data);
?>
</pre>
</noscript>
</div>
<script type=text/javascript>
<!--
// comment out the script for non supporing browsers?!

// helper functions so we can work out if an IPv4 is in a given subnet
// The subnet IP comes from the logical AND of the mask and IP address
// see lookupGeo() for example use.
// googling for this subject led me to http://stackoverflow.com/a/503238

// IPnumber turns a dotted-quad into an integer.
function IPnumber(IPaddress) {
    var ip = IPaddress.match(/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/);
    if(ip) {
        return (+ip[1]<<24) + (+ip[2]<<16) + (+ip[3]<<8) + (+ip[4]);
    }
    // else ... ?
    return null;
}

// IPmask gives the mask integer
function IPmask(maskSize) {
    return -1<<(32-maskSize)
}

// lookup json data for the IP. considering moving this to the PHP so that evenn though it'll still be a call to a server,
// we can memcache it and not hit someone elses server? How much does this get used tho?
// function takes ip address to lookup and element to append to
function lookupGeo(ip,elem) {
	// don't lookup link local!
	var pattern = /^fe80/;
	var check = pattern.test(ip);
	if(check == true) return true;

	// don't lookup localhost, rfc1918, rfc6598
	if((IPnumber(ip) & IPmask('8')) == IPnumber('127.0.0.0')) return true;
	if((IPnumber(ip) & IPmask('8')) == IPnumber('10.0.0.0')) return true;
	if((IPnumber(ip) & IPmask('12')) == IPnumber('172.16.0.0')) return true;
	if((IPnumber(ip) & IPmask('16')) == IPnumber('192.168.0.0')) return true;
	if((IPnumber(ip) & IPmask('10')) == IPnumber('100.64.0.0')) return true;

	// make the call and format the output before appending to the element.
	// using the jain an array technique, as it's quicker/less expensive?
	// http://www.learningjquery.com/2009/03/43439-reasons-to-use-append-correctly/
	$.getJSON("http://ip-api.com/json/" + ip, function(json) {
		var geoToAdd = [];
		var l = 0;
		geoToAdd[l++] = "Lat/Lon: ";
		geoToAdd[l++] = json.lat + ", " + json.lon;
		geoToAdd[l++] = "<br/>";
		geoToAdd[l++] = "Location: ";
		var geoToAddLoc = []
		var g = 0;
		if(json.city) geoToAddLoc[g++] = json.city;
		if(json.regionName) geoToAddLoc[g++] = json.regionName;
		if(json.country) geoToAddLoc[g++] = json.country;
		geoToAdd[l++] = geoToAddLoc.join(', ');
		if(json.countryCode) geoToAdd[l++] = " (" + json.countryCode + ")";
		geoToAdd[l++] = "<br/>";
		geoToAdd[l++] = "AS: ";
		geoToAdd[l++] = json.as;
		geoToAdd[l++] = "<br/>";
		$(elem).append("<h3>GeoIP Data for " + ip + "</h3>").append(geoToAdd.join(''));
	});
}

// commented out as removed clunky
//doneAgent = false; // variable to track if we need to append the agent to the div, as we only wanna do it once for dual stack clients

// function that chucks out the element header plus a spinner
function spinner(elem, head) {
	$(elem).html('<h2>' + head + '</h2><img src="/loading.gif">');
}

// function that takes the protocol (4 or 6), element, and whether to clear the element before building it up
function getIP(prot,elem,clear) {
	// chuck out a loading spinner
	//$(elem).html($('<img/>', { 'src' : '/loading.gif' }));
//	$(elem).html('<h2>IPv' + prot + '</h2><img src="/loading.gif">');
	spinner(elem, 'IPv' + prot);

	var host = [];
	host[4] = <? echo $fourPrefix; ?>;
	host[6] = <? echo $sixPrefix; ?>;
	var h = host[prot];

	// make the call...
	$.getJSON("http://" + h + '.<? echo $siteHost; ?><? if($sitePath) echo "/$sitePath"; ?>?json', function(json) {
		// immediatly kick off the geo lookup for that IP
		lookupGeo(json.REMOTE_ADDR,elem + 'geo');

		// clear the element if needed, and then populate with data
		if(clear === true) $(elem).html('<h2>IPv' + prot + '</h2>');
		if(json.REMOTE_ADDR !== undefined) $(elem).append("<h3>Your IPv" + prot + " address is <a target=_blank href=https://apps.db.ripe.net/search/query.html?searchtext=" + json.REMOTE_ADDR + ">" + json.REMOTE_ADDR + "</a></h3>");
		if(json.REMOTE_ADDR_PTR !== undefined) $(elem).append("DNS says that's " + json.REMOTE_ADDR_PTR).append('<br/>');
		//if(json.COUNTRY_NAME !== undefined) $(elem).append("GeoIP says that's in " + json.COUNTRY_NAME).append(' (').append(json.COUNTRY).append(')<br/>');
		if(json.HTTP_VIA !== undefined) { // we're using http-via to detect if we're proxied
			$(elem).append("That IP seems to be a proxy server (" + json.HTTP_VIA + ")");
			if(json.XFF.length > 1) { // if we have more than one x-forarded-for entry, format with unordered list
				$(elem).append(" and it reports the following IP addresses:");
				$(elem).append('<ul/>');
				$.each(json.XFF, function(i,v) {
					ptr = json.XFF_PTR[i];
					lookupGeo(v, elem + 'proxygeo'); // lookup the geo data for a proxy IP
					var l = 0;
					var toAdd = [];
					toAdd[l++] = '<li><a target=_blank href=https://apps.db.ripe.net/search/query.html?searchtext=';
					toAdd[l++] = v;
					toAdd[l++] = '>';
					toAdd[l++] = v;
					toAdd[l++] = '</a>';
					if(ptr !== undefined && ptr !== null) {
						toAdd[l++] = ' which DNS says is ';
						toAdd[l++] = ptr;
					}
					if(i === 0) {
						toAdd[l++] = ' (This is likely your client)';
					} else {
						toAdd[l++] = ' (This is likely a proxy)';
					}
					toAdd[l++] = '</li>';
					$(elem + " ul").append(toAdd.join(''));
				});
			} else { // else, just chuck it out.
				$(elem).append(" and it says your IP address is <a target=_blank href=https://apps.db.ripe.net/search/query.html?searchtext=" + json.HTTP_X_FORWARDED_FOR + ">" + json.HTTP_X_FORWARDED_FOR + "</a>");
				if(json.XFF_PTR[0] !== undefined && json.XFF_PTR[0] !== null) $(elem).append(" which DNS says is " + json.XFF_PTR[0]);
				$(elem).append(" (This is likely your client");
				if(json.IP_VERSION === 4) $(elem).append(", or the IP it gets NAT'd to");
				$(elem).append(")");
				lookupGeo(json.HTTP_X_FORWARDED_FOR, elem + 'proxygeo');
			}
		}

// moved this to the populate function and populates with navigator.userAgent...
//
//		if(json.HTTP_USER_AGENT !== undefined && doneAgent === false) {
//			$('#agent').html('<h2>User Agent</h2>You are using ' + json.HTTP_USER_AGENT).append('<br/>');
//			doneAgent = true;
//		}

	})
	.fail(function(jq, textStatus, errorThrown) {
		if(clear === true) $(elem).html('<h2>IPv' + prot + '</h2>');
		$(elem).append("Unable to determine IPv" + prot + " address.");
		console.log("Response: " + jq.responseText);
		console.log("Status: " + textStatus);
		console.log("Error: " + errorThrown);
	})
	.always(function() {
		// if we disabled the initial populate, and the refresh button says find, change it to refresh
		$("#refresh").text("Refresh My IP Details");
	})
}

function randomString(length) {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for(var i = 0; i < length; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}


// function that displays the resolver used. relies on ipDns running.
function getResolver() {
	//$('#resolver').html($('<img/>', { 'src' : '/loading.gif' }));
//	$('#resolver').html('<h2>DNS Resolver</h2><img src="/loading.gif">');
	spinner('#resolver', 'DNS Resolver');

	rndstr = randomString(16);
//	randomString = '98798798';
	$.get('http://' + rndstr + '.i.p.c.je/ip?json', function(json) {
		clientString = "";
		if(json.IPDNS.CLIENT !== undefined && json.IPDNS.CLIENT !== false) {
			clientString = '<br/>It said your client subnet is ' + json.IPDNS.CLIENT;
		}
		$('#resolver').html('<h2>DNS Resolver</h2>' + 'Your DNS lookup to get here came from ' + json.IPDNS.SOURCE + clientString);
	})
	.fail(function(jq, textStatus, errorThrown) {
		$('#resolver').html('<h2>DNS Resolver</h2>Unable to determine your DNS resolver\'s IP')
	});
}

// function that populates the content div with the sub divs and content.
function populate() {
	var l=0;
	var toAdd = [];
	toAdd[l++] = '<span id=refresh class="button<? if(preg_match('/(iPad|iPhone|BlackBerry)/', $_SERVER['HTTP_USER_AGENT'])) echo " big"; ?>">Find My IP Details</span>';
	toAdd[l++] = '<div id=four></div>';
	toAdd[l++] = '<div id=fourgeo></div>';
	toAdd[l++] = '<div id=fourproxygeo></div>';
	toAdd[l++] = '<div id=six></div>';
	toAdd[l++] = '<div id=sixgeo></div>';
	toAdd[l++] = '<div id=sixproxygeo></div>';
	toAdd[l++] = '<div id=verdict></div>';
	toAdd[l++] = '<div id=resolver></div>';
	toAdd[l++] = '<div id=agent></div>';
	toAdd[l++] = '<div id=notes>'
	toAdd[l++] = "<h2>Note/caveat</h2>It's only possible to be 100% sure about a proxy in your path if it gives itself away with certain headers such as HTTP_VIA, HTTP_X_FORWARDED_FOR etc.<br/>If a proxy is configured to not add, or indeed remove these entirely, it's not possible to detect them reliably.<br/>This page doesn't check source IPs against lists or RBLs, for example, to see if the IP is likely to be a proxy, it relies on the headers.";
	toAdd[l++] = '</div>';
	$('#content').html(toAdd.join(''));

	// we can set the parameters for the underlying jQuery.ajax() call when using getJSON()
	$.ajaxSetup({
	//	async: false,
		timeout: 3000
	});

	// do the IP lookup calls
	getIP(4,'#four',true);
	getIP(6,'#six',true);
	getResolver();

	$('#agent').html('<h2>User Agent</h2>You are using ...<br/>' + navigator.userAgent).append('<br/><br/>');
	$.get('http://c.je/ip?json', function(json) {
		if(json.HTTP_USER_AGENT !== undefined && json.HTTP_USER_AGENT !== navigator.userAgent) {
			$('#agent').append('This differed from what the web server sees...:</br>' + json.HTTP_USER_AGENT);
		}
	});

	// do a generic call to try and detect whether the client prefers 4 or 6
	$.getJSON('<? echo $siteURL; ?>?json', function(json) {
		$('#verdict').html('<h2>Verdict</h2>Your browser and/or proxy combination <i>seems</i> to prefer IPv' + json.IP_VERSION).append(' for talking to the internet.<br/>The output above will give you more clues, for example, it may show you talk to a proxy via IPv6 that is unable to talk to the internet via IPv4, or which has an IPv4 only upstream.<br/>');
	});

	// set up the refresh button clickery
	$("#refresh").click(function() {
		doneAgent = false;
		populate();
	});
}

// do the initial population.
populate();
//-->
</script>
</body>
</html>
