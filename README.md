
Partly inspired by wanting to learn more about making ajax calls, and write some javascript
and partly inspired by a mailing list thread on "can a webpage simultaneously display both
IPv4 & IPv6 for dual stack clients."

Comments, feedback welcome.

* Curl, WGet & libwww (LWP) get a plain text output with no HTML. Useful from the CLI.
* MSIE 6,7,8 & 9 get plain text output.
* Browsers that don't support javascript (links, lynx, etc) or have it disabled, get plain text
* Regular browsers that use ?plain text query string get plain text
* ?json query string gets the output in JSON format
* Regular browsers get IPv4 & IPv6 in one screen, GeoIP details for IPs including proxies etc
  and detection of whether a browser prefers A or AAAA given the choice.

Requires:

* The country code file as listed in the code; I basically grabbed the info kindly supplied
  in this SO answer http://stackoverflow.com/a/5396003 and chucked it in a file.
* memcached daemon, and php5 support (apt-get install memcached php5-memcached)
